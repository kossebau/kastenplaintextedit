/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EDITFILERELOADTHREAD_H
#define EDITFILERELOADTHREAD_H

// Qt
#include <QByteArray>
#include <QThread>

class QFile;


namespace Kasten
{

class PlaintextFileReloadThread : public QThread
{
  Q_OBJECT

  public:
    PlaintextFileReloadThread( QObject* parent, QFile* file );

    virtual ~PlaintextFileReloadThread();

  public: // QThread API
    virtual void run();

  public:
    bool success() const;
    const QString& errorString() const;

    const QString& text() const;

  Q_SIGNALS:
    void documentReloaded( bool success );

  protected:
    QFile* mFile;

    bool mSuccess;
    QString mErrorString;

    QString mText;
};


inline bool PlaintextFileReloadThread::success()                const { return mSuccess; }
inline const QString& PlaintextFileReloadThread::errorString()  const { return mErrorString; }
inline const QString& PlaintextFileReloadThread::text()                 const { return mText; }

}

#endif
