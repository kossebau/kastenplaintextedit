/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EDITFILECONNECTJOB_H
#define EDITFILECONNECTJOB_H

// Kasten core
#include <Kasten/AbstractFileSystemConnectJob>


namespace Kasten
{

class PlaintextFileSynchronizer;
class PlaintextDocument;
// class PlaintextFileWriteThread;

class PlaintextFileConnectJob : public AbstractFileSystemConnectJob
{
  Q_OBJECT

  public:
    PlaintextFileConnectJob( PlaintextFileSynchronizer *synchronizer, AbstractDocument* document,
                                const QUrl& url, AbstractModelSynchronizer::ConnectOption option );
    virtual ~PlaintextFileConnectJob();

  protected: // AbstractFileSystemLoadJob API
    virtual void startConnectWithFile();

  protected Q_SLOTS:
//     void onDataPulled();

  protected:
//     PlaintextFileWriteThread *mWriteThread;
};

}

#endif
