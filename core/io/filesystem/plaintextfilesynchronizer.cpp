/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "plaintextfilesynchronizer.h"

// lib
#include "plaintextfileloadjob.h"
#include "plaintextfileconnectjob.h"
#include "plaintextfilewritejob.h"
#include "plaintextfilereloadjob.h"
#include "plaintextfilewritetojob.h"
#include "plaintextdocument.h"
// Qt
#include <QTextDocument>
#include <QUrl>


namespace Kasten
{

PlaintextFileSynchronizer::PlaintextFileSynchronizer()
 : mDocument( 0 )
{
    connect( this, SIGNAL(urlChanged(QUrl)), SLOT(onUrlChange(QUrl)) );
}

AbstractDocument* PlaintextFileSynchronizer::document() const { return mDocument; }

void PlaintextFileSynchronizer::startOffering( AbstractDocument* document ) { Q_UNUSED(document) }

AbstractLoadJob* PlaintextFileSynchronizer::startLoad( const QUrl& url )
{
    return new PlaintextFileLoadJob( this, url );
}

AbstractSyncToRemoteJob* PlaintextFileSynchronizer::startSyncToRemote()
{
    return new PlaintextFileWriteJob( this );
}

AbstractSyncFromRemoteJob* PlaintextFileSynchronizer::startSyncFromRemote()
{
    return new PlaintextFileReloadJob( this );
}

AbstractSyncWithRemoteJob* PlaintextFileSynchronizer::startSyncWithRemote( const QUrl& url, AbstractModelSynchronizer::ConnectOption option  )
{
    return new PlaintextFileWriteToJob( this, url, option );
}

AbstractConnectJob* PlaintextFileSynchronizer::startConnect( AbstractDocument* document,
                                              const QUrl& url, AbstractModelSynchronizer::ConnectOption option )
{
    return new PlaintextFileConnectJob( this, document, url, option );
}

LocalSyncState PlaintextFileSynchronizer::localSyncState() const
{
    return mDocument ?
        (mDocument->textDocument()->isModified() ? LocalHasChanges : LocalInSync) : LocalInSync;
}

void PlaintextFileSynchronizer::setDocument( PlaintextDocument* document )
{
    mDocument = document;
    if( mDocument )
        connect( mDocument->textDocument(), SIGNAL(modificationChanged(bool)),
                 SLOT(onTextDocumentModificationChanged(bool)) );

}

void PlaintextFileSynchronizer::onUrlChange( const QUrl& url )
{
    mDocument->setTitle( url.fileName() );
}

void PlaintextFileSynchronizer::onTextDocumentModificationChanged( bool isModified )
{
    emit localSyncStateChanged( (isModified ? LocalHasChanges : LocalInSync) );
}

}
