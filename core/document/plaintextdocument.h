/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EDITDOCUMENT_H
#define EDITDOCUMENT_H

// lib
#include <plaintextkastencore_export.h>
// Kasten core
#include <Kasten/AbstractDocument>

class QTextDocument;


namespace Kasten
{
// TODO: interface GameRun with state Started/Paused/Ended ? Difference to Audio/VideoPlay?
class PLAINTEXTKASTENCORE_EXPORT PlaintextDocument : public AbstractDocument
{
    Q_OBJECT

  public:
    PlaintextDocument();
    explicit PlaintextDocument( const QString &text );
    virtual ~PlaintextDocument();

  public: // AbstractModel API
    virtual QString title() const;

  public: // AbstractDocument API
    virtual QString typeName() const;
    virtual QString mimeType() const;
    virtual ContentFlags contentFlags() const;

  public:
    QTextDocument* textDocument() const;
    void setText( const QString &text );

    void setTitle( const QString &title );

  protected:
    void init();

  private Q_SLOTS:
    void onTextDocumentModificationChanged( bool changed );

  private:
    mutable QString mTitle;

    QTextDocument *mTextDocument;
};

}

#endif
