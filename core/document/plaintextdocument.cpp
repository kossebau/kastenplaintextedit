/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "plaintextdocument.h"

// Qt
#include <QTextDocument>

namespace Kasten
{

PlaintextDocument::PlaintextDocument()
  : mTextDocument( new QTextDocument(this) )
{
    init();
}

PlaintextDocument::PlaintextDocument( const QString &text )
  : mTextDocument( new QTextDocument(text, this) )
{
    init();
}

void PlaintextDocument::init()
{
    mTextDocument->setModified(false);
    connect( mTextDocument, SIGNAL(modificationChanged(bool)), SLOT(onTextDocumentModificationChanged(bool)));
}

QString PlaintextDocument::title() const { return mTitle; }
QString PlaintextDocument::mimeType() const
{ static QString mimeType = QLatin1String("PlaintextDocument"); return mimeType; }
QString PlaintextDocument::typeName() const { return tr("Edit State",  "name of the data type"); }
ContentFlags PlaintextDocument::contentFlags() const
{
    return (mTextDocument->isModified() ? ContentHasUnstoredChanges : ContentStateNormal);
}

QTextDocument* PlaintextDocument::textDocument() const
{
    return mTextDocument;
}

void PlaintextDocument::setText( const QString &text )
{
    mTextDocument->setPlainText(text);
    mTextDocument->setModified(false);
}

void PlaintextDocument::setTitle( const QString &title )
{
    mTitle = title;
    emit titleChanged( mTitle );
}


void PlaintextDocument::onTextDocumentModificationChanged(bool changed)
{
    emit contentFlagsChanged( (changed ? ContentHasUnstoredChanges : ContentStateNormal) );
}

PlaintextDocument::~PlaintextDocument()
{
}

}
