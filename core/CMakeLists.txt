project(plaintextkastencore)

include_directories(
  document
)

set( plaintextkasten_document_SRCS
  document/plaintextdocumentfactory.cpp
  document/plaintextdocument.cpp
)

set( plaintextkasten_io_filesystem_SRCS
  io/filesystem/plaintextfilewritethread.cpp
  io/filesystem/plaintextfileloadthread.cpp
  io/filesystem/plaintextfilereloadthread.cpp
  io/filesystem/plaintextfileconnectjob.cpp
  io/filesystem/plaintextfilewritejob.cpp
  io/filesystem/plaintextfilewritetojob.cpp
  io/filesystem/plaintextfileloadjob.cpp
  io/filesystem/plaintextfilereloadjob.cpp
  io/filesystem/plaintextfilesynchronizer.cpp
  io/filesystem/plaintextfilesynchronizerfactory.cpp
)

set( plaintextkasten_io_SRCS
  ${plaintextkasten_io_filesystem_SRCS}
)
set( plaintextkasten_core_LIB_SRCS
  ${plaintextkasten_document_SRCS}
  ${plaintextkasten_io_SRCS}
)

add_library( ${plaintextkasten_core_LIB}  SHARED ${plaintextkasten_core_LIB_SRCS} )

target_link_libraries( ${plaintextkasten_core_LIB}
  KastenCore
)
set_target_properties( ${plaintextkasten_core_LIB}  PROPERTIES
  VERSION   ${PLAINTEXTKASTEN_LIB_VERSION}
  SOVERSION ${PLAINTEXTKASTEN_LIB_SOVERSION}
)

install( TARGETS ${plaintextkasten_core_LIB}  ${INSTALL_TARGETS_DEFAULT_ARGS} )
