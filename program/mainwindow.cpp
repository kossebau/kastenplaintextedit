/*
    This file is part of the Kasten Plaintext Edit program, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"

// program
#include "program.h"
#include <plaintextview.h>
// Kasten controllers
#include <Kasten/FullScreenController>
#include <Kasten/ModifiedBarController>
#include <Kasten/ReadOnlyController>
#include <Kasten/ReadOnlyBarController>
#include <Kasten/CreatorController>
#include <Kasten/LoaderController>
#include <Kasten/CloseController>
#include <Kasten/SetRemoteController>
#include <Kasten/SynchronizeController>
#include <Kasten/ClipboardController>
#include <Kasten/QuitController>
// Kasten gui
#include <Kasten/SingleDocumentStrategy>
#include <Kasten/ViewManager>
#include <Kasten/StatusBar>
// Kasten core
#include <Kasten/DocumentCreateManager>
#include <Kasten/DocumentSyncManager>
#include <Kasten/DocumentManager>


namespace Kasten
{

static const char LoadedUrlsKey[] = "LoadedUrls";

// TODO: make this a single window, without
// also make a old-fashioned version, which preloads new created game instance
// and stays alive if played game is closed or new instance is requested
PlaintextEditMainWindow::PlaintextEditMainWindow( PlaintextEditProgram* program )
  : SingleViewWindow( 0 )
  , mProgram( program )
{
    setObjectName( QLatin1String("Shell") );

    ViewManager* const viewManager = program->viewManager();
    connect( viewManager, SIGNAL(opened(QList<Kasten::AbstractView*>)),
             SLOT(onViewsOpened(QList<Kasten::AbstractView*>)) );
    connect( viewManager, SIGNAL(closing(QList<Kasten::AbstractView*>)),
             SLOT(onViewsClosing(QList<Kasten::AbstractView*>)) );

    setStatusBar( new Kasten::StatusBar(this) );

    setupControllers();
    setupGUI();
}

void PlaintextEditMainWindow::setupControllers()
{
    SingleDocumentStrategy* const documentStrategy = mProgram->documentStrategy();
    DocumentManager* const documentManager = mProgram->documentManager();
    ModelCodecManager* const codecManager = documentManager->codecManager();
    DocumentSyncManager* const syncManager = documentManager->syncManager();

    // general, part of Kasten
    addXmlGuiController( new CreatorController(codecManager,
                                               documentStrategy,this) );
    addXmlGuiController( new LoaderController(documentStrategy,this) );
    addXmlGuiController( new SetRemoteController(syncManager,this) );
    addXmlGuiController( new SynchronizeController(syncManager,this) );
    addXmlGuiController( new CloseController(documentStrategy,this) );
    addXmlGuiController( new FullScreenController(this) );
    addXmlGuiController( new ReadOnlyController(this) );
    addXmlGuiController( new QuitController(0,this) );

    Kasten::StatusBar* const bottomBar = static_cast<Kasten::StatusBar*>( statusBar() );
    addXmlGuiController( new ModifiedBarController(bottomBar) );
}

bool PlaintextEditMainWindow::queryClose()
{
    AbstractView* view = this->view();
    AbstractDocument* document = view ? view->findBaseModel<AbstractDocument*>() : 0;
    // TODO: query the document manager or query the view manager?
    return (! document) || mProgram->documentManager()->canClose( document );
}

void PlaintextEditMainWindow::onViewsOpened( const QList<AbstractView*>& views )
{
    if( ! views.isEmpty() && view() == 0 )
    {
        AbstractView* const view = views.at( 0 );
        setView( view );
    }
}

void PlaintextEditMainWindow::onViewsClosing( const QList<AbstractView*>& views )
{
    AbstractView* const oldView = view();

    if( ! views.isEmpty() && oldView != 0 )
    {
        AbstractView* const view = views.at( 0 );
        if( oldView == view )
            setView( 0 );
    }

}

PlaintextEditMainWindow::~PlaintextEditMainWindow() {}

}
