/*
    This file is part of the Kasten Plaintext Edit program, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAINTEXTEDITMAINWINDOW_H
#define PLAINTEXTEDITMAINWINDOW_H

// Kasten gui
#include <Kasten/SingleViewWindow>


namespace Kasten
{

class PlaintextEditProgram;
class DocumentManager;

class PlaintextEditMainWindow : public SingleViewWindow
{
   Q_OBJECT

  public:
    explicit PlaintextEditMainWindow( PlaintextEditProgram* program );
    virtual ~PlaintextEditMainWindow();

  protected: // KMainWindow API
    virtual bool queryClose();

  protected Q_SLOTS:
    void onViewsOpened( const QList<Kasten::AbstractView*>& views );
    void onViewsClosing( const QList<Kasten::AbstractView*>& views );

  protected:
    void setupControllers();

  protected:
    PlaintextEditProgram* mProgram;
};

}

#endif
