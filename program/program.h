/*
    This file is part of the Kasten Plaintext Edit program, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAINTEXTEDITPROGRAM_H
#define PLAINTEXTEDITPROGRAM_H

// Qt
#include <QApplication>


namespace Kasten
{
class DialogHandler;
class DocumentManager;
class ViewManager;
class SingleDocumentStrategy;


class PlaintextEditProgram
{
  public:
    PlaintextEditProgram( int &argc, char* argv[] );
    ~PlaintextEditProgram();

  public:
    int execute();
    void quit();

  public:
    DocumentManager* documentManager();
    ViewManager* viewManager();
    SingleDocumentStrategy* documentStrategy();

  protected:
    QApplication mApp;

    DocumentManager* mDocumentManager;
    ViewManager* mViewManager;
    SingleDocumentStrategy* mDocumentStrategy;

    DialogHandler* mDialogHandler;
};


inline DocumentManager* PlaintextEditProgram::documentManager()         { return mDocumentManager; }
inline ViewManager* PlaintextEditProgram::viewManager()                 { return mViewManager; }
inline SingleDocumentStrategy* PlaintextEditProgram::documentStrategy() { return mDocumentStrategy; }

}

#endif
