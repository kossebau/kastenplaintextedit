/*
    This file is part of the Kasten Plaintext Edit program, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of
    the License or (at your option) version 3 or any later version
    accepted by the membership of KDE e.V. (or its successor approved
    by the membership of KDE e.V.), which shall act as a proxy 
    defined in Section 14 of version 3 of the license.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "program.h"

// program
#include "about.h"
#include "mainwindow.h"
// Edit Kasten
#include <plaintextviewfactory.h>
#include <plaintextdocumentfactory.h>
#include <filesystem/plaintextfilesynchronizerfactory.h>
// Kasten gui
#include <Kasten/SingleDocumentStrategy>
#include <Kasten/DialogHandler>
#include <Kasten/ViewManager>
// Kasten core
#include <Kasten/DocumentManager>
#include <Kasten/DocumentCreateManager>
#include <Kasten/DocumentSyncManager>
// KF5
#include <KLocalizedString>
// Qt
#include <QCommandLineParser>
#include <QList>
#include <QUrl>


namespace Kasten
{

PlaintextEditProgram::PlaintextEditProgram( int &argc, char* argv[] )
  : mApp( argc, argv )
{
}


int PlaintextEditProgram::execute()
{
    mDocumentManager = new DocumentManager();
    mViewManager = new ViewManager();
    mDocumentStrategy = new SingleDocumentStrategy(mDocumentManager, mViewManager);
    mDialogHandler = new DialogHandler();

    EditAboutData aboutData;
    KAboutData::setApplicationData( aboutData );

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption createOption(QStringList() << "c" << "create",
            i18n("Create a new Edit."));
    parser.addOption(createOption);
    QCommandLineOption generatorOption(QStringList() << "g" << "generator",
            i18n("Define the generator for creating the new Edit."),
            i18n("name"));
    parser.addOption(generatorOption);

    // urls to open
    parser.addPositionalArgument( QStringLiteral("urls"), i18n("File(s) to load."), QStringLiteral("[urls...]") );

    parser.process(mApp);

    aboutData.processCommandLine(&parser);

    mDocumentManager->createManager()->setDocumentFactory( new PlaintextDocumentFactory() );
    mDocumentManager->syncManager()->setDocumentSynchronizerFactory( new PlaintextFileSynchronizerFactory() );
    mDocumentManager->syncManager()->setOverwriteDialog( mDialogHandler );
    mDocumentManager->syncManager()->setSaveDiscardDialog( mDialogHandler );
    mViewManager->setViewFactory( new PlaintextViewFactory() );

    PlaintextEditMainWindow* mainWindow = new PlaintextEditMainWindow( this );
    mDialogHandler->setWidget( mainWindow );

    // started by session management?
    if( mApp.isSessionRestored() && KMainWindow::canBeRestored(1) )
    {
        mainWindow->restore( 1 );
    }
    else
    {
        // no session.. just start up normally
        const QStringList urls = parser.positionalArguments();

        // take arguments
        if( parser.isSet(createOption) )
        {
            const QString generatorId =
                parser.value(generatorOption);
            if( generatorId.isEmpty() )
                mDocumentStrategy->createNew();
            else if( generatorId == QLatin1String("FromClipboard") )
                mDocumentStrategy->createNewFromClipboard();
        }
        else if( ! urls.isEmpty() )
        {
            const QRegExp withProtocolChecker( QStringLiteral("^[a-zA-Z]+:") );
            foreach (const QString &url, urls) {
                const QUrl u = (withProtocolChecker.indexIn(url) == 0) ?
                    QUrl::fromUserInput( url ) : QUrl::fromLocalFile( url );
                mDocumentStrategy->load( u );
            }
        }

        mainWindow->show();
    }

    return mApp.exec();
}


void PlaintextEditProgram::quit()
{
    QCoreApplication::instance()->quit();
}


PlaintextEditProgram::~PlaintextEditProgram()
{
    delete mDocumentStrategy;
    delete mDocumentManager;
    delete mViewManager;
    delete mDialogHandler;
}

}
