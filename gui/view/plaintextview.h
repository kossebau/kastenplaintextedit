/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAINTEXTVIEW_H
#define PLAINTEXTVIEW_H

// lib
#include <plaintextkastengui_export.h>
//
// Kasten gui
#include <Kasten/DataSelectable>
#include <Kasten/AbstractView>

class QTextEdit;


namespace Kasten
{

class PlaintextDocument;


class PLAINTEXTKASTENGUI_EXPORT PlaintextView : public AbstractView,
                                                public If::DataSelectable
{
    Q_OBJECT
    Q_INTERFACES( Kasten::If::DataSelectable )

  public:
    explicit PlaintextView( PlaintextDocument* document );
    virtual ~PlaintextView();

  public: // AbstractModel API
    virtual QString title() const;
    virtual bool isModifiable() const;
    virtual bool isReadOnly() const;
    virtual void setReadOnly( bool isReadOnly );

  public: // AbstractView API
    virtual void setFocus();
    virtual QWidget* widget() const;
    virtual bool hasFocus() const;

  public: // If::DataSelectable API
    virtual void selectAllData( bool selectAll );
    virtual bool hasSelectedData() const;
    virtual QMimeData* copySelectedData() const;
    virtual const AbstractModelSelection* modelSelection() const;
//     virtual void setSelection();
  Q_SIGNALS:
    /*virtual*/ void hasSelectedDataChanged( bool hasSelectedData );
    /*virtual*/ void selectedDataChanged( const Kasten::AbstractModelSelection* modelSelection );

  protected:
    QTextEdit* mWidget;
    PlaintextDocument* mDocument;
};

}

#endif
