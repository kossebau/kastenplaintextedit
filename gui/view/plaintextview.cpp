/*
    This file is part of the Plaintext Kasten Framework, made within the KDE community.

    Copyright 2013 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library. If not, see <http://www.gnu.org/licenses/>.
*/

#include "plaintextview.h"

//
#include <plaintextdocument.h>
// Qt
#include <QTextEdit>

namespace Kasten
{

PlaintextView::PlaintextView( PlaintextDocument* document )
  : AbstractView( document ),
    mDocument( document )
{
    mWidget = new QTextEdit();
    mWidget->setDocument( document->textDocument() );

    // propagate signals
    connect( mDocument, SIGNAL(titleChanged( QString )), SIGNAL(titleChanged( QString )) );
//     connect( mWidget, SIGNAL(selectionChanged( bool )), SIGNAL(hasSelectedDataChanged( bool )) );
}

const AbstractModelSelection* PlaintextView::modelSelection() const { return 0;}//&mSelection; }

bool PlaintextView::hasFocus() const { return mWidget->focusWidget()->hasFocus(); }

QWidget* PlaintextView::widget()             const { return mWidget; }
QString PlaintextView::title()               const { return mDocument->title(); }
bool PlaintextView::isModifiable()           const { return true; }
bool PlaintextView::isReadOnly()             const { return mWidget->isReadOnly(); }

void PlaintextView::setFocus() { mWidget->setFocus(); }

void PlaintextView::setReadOnly( bool isReadOnly ) { mWidget->setReadOnly( isReadOnly ); }

void PlaintextView::selectAllData( bool selectAll )
{
Q_UNUSED( selectAll )
//     mWidget->selectAll( selectAll );
}

bool PlaintextView::hasSelectedData() const
{
    return false;//mWidget->hasSelectedData();
}

QMimeData* PlaintextView::copySelectedData() const
{
    return 0; //mWidget->selectionAsMimeData();
}


#if 0
void PlaintextView::onSelectionChange( bool selected )
{
    mSelection.setSection( mWidget->selection() );
    emit hasSelectedDataChanged( selected );
}
#endif


PlaintextView::~PlaintextView()
{
    delete mWidget;
}

}
